//
//  ImageViewController.swift
//  VACarousal
//
//  Created by Vikash Anand on 03/03/19.
//  Copyright © 2019 Vikash Anand. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController {

    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var profilePictureView: UIImageView!
    let cellScale: CGFloat = 0.6
    let architectures = Architecture.getArchitectures()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupProfilePictureView()
        self.setupCollectionView()
    }
    
    private func setupCollectionView() {
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.collectionView.collectionViewLayout = ZoomAndSnapFlowLayout()
    }
    
    private func setupProfilePictureView() {
        self.profilePictureView.layer.cornerRadius = self.profilePictureView.bounds.width / 2.0
        self.profilePictureView.layer.masksToBounds = true
    }
}

extension ImageViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.architectures.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as! ArchitectureViewCell
        let architecture = self.architectures[indexPath.row]
        cell.architecture = architecture
        
        return cell
    }
}

extension ImageViewController: UIScrollViewDelegate, UICollectionViewDelegate {
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        let layout = self.collectionView?.collectionViewLayout as! UICollectionViewFlowLayout
        let cellWidthIncludingSpacing = layout.itemSize.width + layout.minimumLineSpacing
        var offset = targetContentOffset.pointee
        let index = (offset.x + scrollView.contentInset.left) / cellWidthIncludingSpacing
        let roundedIndex = round(index)
        
        offset = CGPoint(x: roundedIndex * cellWidthIncludingSpacing - scrollView.contentInset.left, y: scrollView.contentInset.top)
        
        targetContentOffset.pointee = offset
    }
}

