//
//  ArchitectureViewCell.swift
//  VACarousal
//
//  Created by Vikash Anand on 04/03/19.
//  Copyright © 2019 Vikash Anand. All rights reserved.
//

import UIKit

class ArchitectureViewCell: UICollectionViewCell {
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var coloredView: UIView!
    @IBOutlet var label: UILabel!
    
    var architecture: Architecture? {
        didSet {
            self.updateUI()
        }
    }
    
    private func updateUI() {
        
        if let architecture = self.architecture {
            self.imageView.image = architecture.image
            self.coloredView.backgroundColor = architecture.tintColor
            self.label.text = architecture.pictureClickedBy
        }
        
        self.imageView.layer.cornerRadius = 10.0
        self.imageView.layer.masksToBounds = true
        self.coloredView.layer.cornerRadius = 10.0
        self.coloredView.layer.masksToBounds = true
    }
}
